﻿using System.Collections.Generic;
using UnityEngine;

namespace BeyondLight
{
	public class LodCameraController : MonoBehaviour
	{
		private Camera follow;

		[SerializeField]
		private Vector3 Goal;
		[SerializeField]
		private Vector3 Position;

		private List<Camera> cameras;
		private List<float> scales;
		private float scaleFactor3;

		// Start is called before the first frame update
		void Start()
		{
			follow = GameObject.Find("OrbitCam").GetComponent<Camera>();

			scaleFactor3 = GameObject.Find("Lod3").GetComponent<LodScale>().ScaleFactor;

			Goal = follow.transform.position / scaleFactor3;

			Position = follow.transform.position / scaleFactor3;

			cameras = new List<Camera>();
			scales = new List<float>();

			cameras.Add(GameObject.Find("Map").transform.Find("Camera").GetComponent<Camera>());
			cameras.Add(GameObject.Find("Lod1").transform.Find("LargeCam").GetComponent<Camera>());
			cameras.Add(GameObject.Find("Lod2").transform.Find("MediumCam").GetComponent<Camera>());

			scales.Add(0.0f);
			scales.Add(GameObject.Find("Lod1").GetComponent<LodScale>().ScaleFactor);
			scales.Add(GameObject.Find("Lod2").GetComponent<LodScale>().ScaleFactor);

		}

		// Update is called once per frame
		void LateUpdate()
		{
			Position = follow.transform.position;
			Goal = follow.transform.position / scaleFactor3;


			for (int i = 0; i < cameras.Count; i++)
			{
				Vector3 scaledPos = Position * scales[i];
				Vector3 scaledGoal = Goal * scales[i];

				if ((scaledPos - scaledGoal).magnitude > 200)
				{
					cameras[i].transform.parent.gameObject.SetActive(false);
				}
				else
				{
					
					cameras[i].transform.parent.gameObject.SetActive(true);

					cameras[i].gameObject.transform.position = scaledPos;
					cameras[i].gameObject.transform.rotation = follow.transform.rotation;
				}
			}
		}
	}
}