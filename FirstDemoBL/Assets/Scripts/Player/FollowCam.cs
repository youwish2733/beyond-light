﻿using UnityEngine;

namespace BeyondLight
{
	public class FollowCam : MonoBehaviour
	{
		[SerializeField]
		private Transform target;
		[SerializeField]
		private Vector3 offset = new Vector3(0f, 2f, -15f);
		[SerializeField]
		private float mouseSensitivity = 4f;
		[SerializeField]
		private float scrollSensitivity = 2f;
		[SerializeField]
		private float orbitSmooth = 10f;
		[SerializeField]
		private float scrollSmooth = 4f;

		private Transform thisCam;
		private bool camDisabled = true;
		private Vector3 velocity = Vector3.zero;
		private Vector3 localRot;
		private float camDistance = 20f;

		void Awake()
		{

		}

		private void Start()
		{
			thisCam = transform;
			thisCam.position = target.position;
		}
		void LateUpdate()
		{
			//localRot = target.position;

			if (Input.GetKey(KeyCode.LeftAlt))
				camDisabled = false;
			else
				camDisabled = true;

			if (camDisabled == false)
			{
				//Rotate the camera based on mouse coordinates.
				if (Input.GetKey(KeyCode.Mouse0))
					if (Input.GetAxis("Mouse X") != 0 || Input.GetAxis("Mouse Y") != 0)
					{
						localRot.x += Input.GetAxis("Mouse X") * mouseSensitivity;
						localRot.y -= Input.GetAxis("Mouse Y") * mouseSensitivity;
					}
			}

			//Zoom the camera based on mouse scroll wheel.
			if (Input.GetAxis("Mouse ScrollWheel") != 0)
			{
				float scrollAmount = Input.GetAxis("Mouse ScrollWheel") * scrollSensitivity;

				//Zoom out faster the further the camera is from the target.
				scrollAmount *= (camDistance * 0.3f);

				camDistance += scrollAmount * -1f;

				//Limit zoom to no closer than 1.5 units from the target, and no further than 100 units.
				camDistance = Mathf.Clamp(camDistance, 10f, 200f);
			}

			Quaternion qt = Quaternion.Euler(localRot.y, localRot.x, 0);

			Quaternion rotation = Quaternion.Lerp(thisCam.rotation, qt, Time.deltaTime * orbitSmooth);

			//Vector3 toPos = target.position + (target.rotation * defaultDistance);
			//Vector3 curPos = Vector3.SmoothDamp(camT.position, toPos, ref velocity, smoothTime);
			//camT.position = curPos;
			//thisCam.LookAt(target, target.up);

			Vector3 negDistance = new Vector3(0.0f, 0.0f, -camDistance);
			thisCam.position = target.position + rotation * negDistance;

			thisCam.rotation = rotation;
		}
	}
}