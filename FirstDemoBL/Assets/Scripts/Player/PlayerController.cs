﻿using UnityEngine;

namespace BeyondLight
{
	public class PlayerController : MonoBehaviour
	{
		[SerializeField]
		private float maxSpeed = 500f;
		[SerializeField]
		private float turnSpeed = 60f;
		[SerializeField]
		private float accRate = 1;
		[SerializeField]
		private float playerSpeed = 0;
		[SerializeField]
		private float speedInterval = 0.2f;

		private bool hasStarted = false;

		private float counter = 0;
		KeyCode accelerate = KeyCode.UpArrow;
		KeyCode decelerate = KeyCode.DownArrow;

		Transform playerT;


		private void Awake()
		{
			playerT = transform;
		}

		// Update is called once per frame
		void FixedUpdate()
		{
			Turn();
			Move();
		}

		// Rotate the ship in 3 different ways to maximize movement control. 
		private void Turn()
		{
			float yaw = turnSpeed * Time.deltaTime * Input.GetAxis("Horizontal");
			float pitch = turnSpeed * Time.deltaTime * Input.GetAxis("Pitch");
			float roll = turnSpeed * Time.deltaTime * Input.GetAxis("Roll");

			playerT.Rotate(pitch, yaw, roll);
		}

		// Player (ship) movement
		private void Move()
		{
			Accelerate();
			Decelerate();
			// Uncomment this line to prevent the player from going outside of the bounds of the space.
			//ClampCameraControl();
		}

		// Accelerate the ship overtime at a constant speed after pressing a button once. 
		// If the button is held down, the ship will continue to accelerate until it reaches maximum speed.
		private void Accelerate()
		{
			if (Input.GetKey(accelerate) || hasStarted == true)
			{
				if (Input.GetKeyDown(accelerate))
				{
					playerSpeed += accRate;
				}
				else if (Input.GetKey(accelerate))
				{
					if (counter < speedInterval)
					{
						counter += Time.deltaTime;
					}
					else
					{
						counter = 0;

						playerSpeed += accRate;
					}
				}
				if (playerSpeed > maxSpeed)
					playerSpeed = maxSpeed;
				playerT.Translate(Vector3.forward * playerSpeed * Time.deltaTime);
				hasStarted = true;
			}
		}

		// Decelerate (slow down) the ship overtime at a constant speed after pressing a button once. 
		// If the button is held down, the ship will continue to decelerate until it stops.
		private void Decelerate()
		{
			if (Input.GetKey(decelerate) || hasStarted == true)
			{
				if (Input.GetKeyDown(decelerate))
				{
					playerSpeed -= accRate;
				}
				else if (Input.GetKey(decelerate))
				{
					if (counter < speedInterval)
					{
						counter += Time.deltaTime;
					}
					else
					{
						counter = 0;

						playerSpeed -= accRate;
					}
				}
				if (playerSpeed < 0)
				{
					playerSpeed = 0;
					hasStarted = false;
				}
				playerT.Translate(Vector3.forward * playerSpeed * Time.deltaTime);
			}
		}

		//void ClampCameraControl()
		//{
		//    Vector3 position = this.transform.position;

		//    if (Universe.UniverseInstance.OpenView == true)
		//    {
		//        position.x = Mathf.Clamp(transform.position.x, -Universe.UniverseInstance.maxRadius, Universe.UniverseInstance.maxRadius);
		//        position.z = Mathf.Clamp(transform.position.z, -Universe.UniverseInstance.maxRadius, Universe.UniverseInstance.maxRadius);
		//        position.y = Mathf.Clamp(transform.position.y, -Universe.UniverseInstance.maxRadius, Universe.UniverseInstance.maxRadius);
		//    }

		//    this.transform.position = position;
		//}
	}
}