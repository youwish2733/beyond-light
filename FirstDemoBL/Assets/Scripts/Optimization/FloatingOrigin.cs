﻿using UnityEngine;

namespace BeyondLight
{
	public class FloatingOrigin : MonoBehaviour
	{
		public float threshold = 100.0f;

		private Activator activationScript;
		private GameObject activationObject;

		private ParticleSystem.Particle[] parts = null;
		private GameObject[] gos;
		private Object[] objects;

		private float lod2Scale;
		private float lod1Scale;
		private GameObject lod2Object;
		private GameObject lod1Object;

		private void Start()
		{
			activationObject = GameObject.Find("ActivatorLarge");
			lod2Object = GameObject.Find("Lod2");
			lod1Object = GameObject.Find("Lod1");

			lod2Scale = lod2Object.GetComponent<LodScale>().ScaleFactor;
			lod1Scale = lod1Object.GetComponent<LodScale>().ScaleFactor;
		}
		// Update is called once per frame
		void Update()
		{
			if (gos == null)
				gos = GameObject.FindObjectsOfType(typeof(GameObject)) as GameObject[];

			activationScript = activationObject.GetComponent<Activator>();

			Vector3 camPosition = gameObject.transform.position;

			if (camPosition.magnitude > threshold)
			{
				for (int i = 0; i < gos.Length; i++)
				{
					if (gos[i].transform.parent && gos[i].transform.parent.CompareTag("Level3"))
					{
						gos[i].transform.position -= camPosition;
					}

					if (gos[i].transform.parent && gos[i].transform.parent.CompareTag("Level2"))
					{
						gos[i].transform.position -= camPosition * lod2Scale;
					}

					if (gos[i].transform.parent && gos[i].transform.parent.CompareTag("Level1"))
					{
						gos[i].transform.position -= camPosition * lod1Scale;
					}
				}

				foreach (ActivatorItem g in activationScript.activatorItems)
				{
					g.objPos -= camPosition * lod1Scale;
				}

				//particles
				if (objects == null)
					objects = FindObjectsOfType(typeof(ParticleSystem));
				foreach (UnityEngine.Object o in objects)
				{
					ParticleSystem sys = (ParticleSystem)o;

					if (sys.main.simulationSpace != ParticleSystemSimulationSpace.World)
						continue;
					int particlesNeeded = sys.main.maxParticles;
					if (particlesNeeded <= 0)
						continue;

					bool wasPaused = sys.isPaused;
					bool wasPlaying = sys.isPlaying;

					if (!wasPaused)
						sys.Pause();

					//Ensure a sufficiently large array in which to store the particles
					if (parts == null || parts.Length < particlesNeeded)
					{
						parts = new ParticleSystem.Particle[particlesNeeded];
					}

					int num = sys.GetParticles(parts);

					for (int j = 0; j < num; j++)
					{
						parts[j].position -= camPosition;
					}

					sys.SetParticles(parts, num);

					if (wasPlaying)
						sys.Play();
				}
			}
		}
	}
}