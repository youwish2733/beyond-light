﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BeyondLight
{
	public class Activator : MonoBehaviour
	{
		[SerializeField]
		private int distance = 1000;

		private GameObject layerCam;
		public List<ActivatorItem> activatorItems;

		// Start is called before the first frame update
		void Start()
		{
			layerCam = GameObject.Find("LargeCam");
			activatorItems = new List<ActivatorItem>();

			StartCoroutine("CheckActivation");
		}

		private IEnumerator CheckActivation()
		{
			List<ActivatorItem> removeList = new List<ActivatorItem>();

			if (activatorItems.Count > 0)
			{
				foreach (ActivatorItem item in activatorItems)
				{
					MeshRenderer[] meshRenderers = item.obj.GetComponentsInChildren<MeshRenderer>() as MeshRenderer[];
					if (Vector3.Distance(layerCam.transform.position, item.objPos) > distance)
					{
						if (item.obj == null)
						{
							removeList.Add(item);
						}
						else
						{
							item.obj.transform.Find("StarClose/Glow").gameObject.SetActive(false);

							foreach (MeshRenderer m in meshRenderers)
							{
								m.enabled = false;
							}

							item.obj.transform.Find("StarFar").gameObject.SetActive(true);
						}
					}
					else
					{
						if (item.obj == null)
						{
							removeList.Add(item);
						}
						else
						{
							item.obj.transform.Find("StarClose/Glow").gameObject.SetActive(true);

							foreach (MeshRenderer m in meshRenderers)
							{
								m.enabled = true;
							}

							item.obj.transform.Find("StarFar").gameObject.SetActive(false);
						}
					}
				}
			}

			yield return new WaitForSeconds(0.01f);

			if (removeList.Count > 0)
			{
				foreach (ActivatorItem item in removeList)
				{
					activatorItems.Remove(item);
				}
			}

			yield return new WaitForSeconds(0.1f);
			StartCoroutine("CheckActivation");
		}
	}

	public class ActivatorItem
	{
		public GameObject obj;
		public Vector3 objPos;
	}
}