﻿using System.Collections;
using UnityEngine;

namespace BeyondLight
{
	public class DisableIfFar : MonoBehaviour
	{
		private GameObject obj;
		private Activator activationScript;
		private int lastPosition;

		// Start is called before the first frame update
		void Start()
		{
			activationScript = GameObject.Find("ActivatorLarge").GetComponent<Activator>();

			activationScript.activatorItems.Add(new ActivatorItem { obj = this.gameObject, objPos = transform.position });
		}
	}
}