﻿using UnityEngine;
using UnityEditor;

namespace BeyondLight.UI
{
	[CustomEditor(typeof(Planet))]
	public class PlanetEditor : Editor
	{
		Planet planet;
		Editor shapeEditor;
		Editor colorEditor;

		public override void OnInspectorGUI()
		{
			using (var check = new EditorGUI.ChangeCheckScope())
			{
				base.OnInspectorGUI();
			}

			if (GUILayout.Button("Generate Planet"))
			{
				planet.GeneratePlanet();
			}
			DrawSettingsEditor(planet.ShapeSettings, planet.OnShapeSettingsUpdated);
			DrawSettingsEditor(planet.ColorSettings, planet.OnColorSettingsUpdated);
		}

		void DrawSettingsEditor(Object settings, System.Action onSettingsUpdated)
		{
			using (var check = new EditorGUI.ChangeCheckScope())
			{
				Editor editor = CreateEditor(settings);
				editor.OnInspectorGUI();

				if (check.changed)
				{
					if (onSettingsUpdated != null)
					{
						onSettingsUpdated();
					}
				}
			}
		}

		private void OnEnable()
		{
			planet = (Planet)target;
		}
	}
}