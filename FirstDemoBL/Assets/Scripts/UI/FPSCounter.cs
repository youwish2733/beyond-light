﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FPSCounter : MonoBehaviour
{
    private const float HUD_REFRESH_RATE = 0.1f;

    private string fpsText;
    private float timer;

    private void Update()
    {
        if (Time.unscaledTime > timer)
        {
            int fps = (int)(1f / Time.unscaledDeltaTime);
            fpsText = fps + " fps";
            timer = Time.unscaledTime + HUD_REFRESH_RATE;
        }
    }

    /// <summary>
    /// Create UI elements for FPS counter.
    /// </summary>
    void OnGUI()
    {
        int w = Screen.width, h = Screen.height;

        GUIStyle style = new GUIStyle();

        Rect rect = new Rect(0, 0, w, h * 2 / 100);
        style.alignment = TextAnchor.UpperRight;
        style.fontSize = h * 2 / 100;
        style.normal.textColor = new Color(1.0f, 1.0f, 1.0f, 1.0f);
        string text = fpsText;
        GUI.Label(rect, text, style);
    }
}
