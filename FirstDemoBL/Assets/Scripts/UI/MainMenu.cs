﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace BeyondLight.UI
{
	public class MainMenu : MonoBehaviour
	{
		private const string CONFIG_EXISTS_KEY = "config_exists";
		private const string SEED_NUMBER_KEY = "seed_number_key";
		private const string RADIUS_KEY = "radius_key";
		private const string NUMBER_OF_STARS_KEY = "number_of_stars_key";
		private const string MIN_DISTANCE_BETWEEN_STARS_KEY = "min_distance_between_stars_key";
		private const string MIN_STAR_SIZE_KEY = "min_star_size_key";
		private const string MAX_STAR_SIZE_KEY = "max_star_size_key";
		private const string MIN_NUMBER_OF_PLANETS_KEY = "min_nr_of_planets_key";
		private const string MAX_NUMBER_OF_PLANETS_KEY = "max_nr_of_planets_key";

		public Button generateButton;

		public Button quitButton;

		public string gameSceneName;

		public InputField seedNumberField;

		public static int seedNumber = 2;

		public InputField radiusField;

		public static int radius = 5000;

		public InputField nrOfStarsField;

		public static int nrOfStars = 5;

		public InputField minDistanceBetweenStarsField;

		public static int minDistanceBetweenStars = 700;

		public InputField minStarSizeField;

		public static int minStarSize = 10;

		public InputField maxStarSizeField;

		public static int maxStarSize = 25;

		public InputField minNrOfPlanetsField;

		public static int minNrOfPlanets = 2;

		public InputField maxNrOfPlanetsField;

		public static int maxNrOfPlanets = 9;

		public void Awake()
		{
			generateButton.onClick.AddListener(Generate);
			quitButton.onClick.AddListener(Quit);
		}

		private void Start()
		{
			// Check if a configuration already exists.
			if (PlayerPrefs.HasKey(CONFIG_EXISTS_KEY))
            {
                LoadConfiguration();
			}
			else
			{
				InitConfiguration();

            }
		}
		
		private void InitConfiguration()
		{
            seedNumberField.text = "2";

            radiusField.text = "5000";

            nrOfStarsField.text = "5";

            minDistanceBetweenStarsField.text = "700";

            minStarSizeField.text = "10";

            maxStarSizeField.text = "25";

            minNrOfPlanetsField.text = "2";

            maxNrOfPlanetsField.text = "9";
        }

		private void LoadConfiguration()
		{
            seedNumber = int.Parse(PlayerPrefs.GetString(SEED_NUMBER_KEY));
            seedNumberField.text = seedNumber.ToString();

            radius = int.Parse(PlayerPrefs.GetString(RADIUS_KEY));
            radiusField.text = radius.ToString();

            nrOfStars = int.Parse(PlayerPrefs.GetString(NUMBER_OF_STARS_KEY));
            nrOfStarsField.text = nrOfStars.ToString();

            minDistanceBetweenStars = int.Parse(PlayerPrefs.GetString(MIN_DISTANCE_BETWEEN_STARS_KEY));
            minDistanceBetweenStarsField.text = minDistanceBetweenStars.ToString();

            minStarSize = int.Parse(PlayerPrefs.GetString(MIN_STAR_SIZE_KEY));
            minStarSizeField.text = minStarSize.ToString();

            maxStarSize = int.Parse(PlayerPrefs.GetString(MAX_STAR_SIZE_KEY));
            maxStarSizeField.text = maxStarSize.ToString();

            minNrOfPlanets = int.Parse(PlayerPrefs.GetString(MIN_NUMBER_OF_PLANETS_KEY));
            minNrOfPlanetsField.text = minNrOfPlanets.ToString();

            maxNrOfPlanets = int.Parse(PlayerPrefs.GetString(MAX_NUMBER_OF_PLANETS_KEY));
            maxNrOfPlanetsField.text = maxNrOfPlanets.ToString();
        }

		private void Generate()
		{
			SaveConfiguration();
            SceneManager.LoadScene(gameSceneName);
		}

		private void SaveConfiguration()
		{
			seedNumber = int.Parse(seedNumberField.text);
			PlayerPrefs.SetString(SEED_NUMBER_KEY, seedNumberField.text);

			radius = int.Parse(radiusField.text);
			PlayerPrefs.SetString(RADIUS_KEY, radiusField.text);

			nrOfStars = int.Parse(nrOfStarsField.text);
			PlayerPrefs.SetString(NUMBER_OF_STARS_KEY, nrOfStarsField.text);

			minDistanceBetweenStars = int.Parse(minDistanceBetweenStarsField.text);
			PlayerPrefs.SetString(MIN_DISTANCE_BETWEEN_STARS_KEY, minDistanceBetweenStarsField.text);

			minStarSize = int.Parse(minStarSizeField.text);
			PlayerPrefs.SetString(MIN_STAR_SIZE_KEY, minStarSizeField.text);

			maxStarSize = int.Parse(maxStarSizeField.text);
			PlayerPrefs.SetString(MAX_STAR_SIZE_KEY, maxStarSizeField.text);

			minNrOfPlanets = int.Parse(minNrOfPlanetsField.text);
			PlayerPrefs.SetString(MIN_NUMBER_OF_PLANETS_KEY, minNrOfPlanetsField.text);

			maxNrOfPlanets = int.Parse(maxNrOfPlanetsField.text);
			PlayerPrefs.SetString(MAX_NUMBER_OF_PLANETS_KEY, maxNrOfPlanetsField.text);

			Debug.Log("Seed number = " + seedNumber);
			Debug.Log("Radius = " + radius);
			Debug.Log("Number of stars = " + nrOfStars);
			Debug.Log("Minimum distance between stars = " + minDistanceBetweenStars);
			Debug.Log("Minimum star size = " + minStarSize);
			Debug.Log("Maximum star size = " + maxStarSize);
			Debug.Log("Minimum number of planets in a system = " + minNrOfPlanets);
			Debug.Log("Maximum number of planets in a system = " + maxNrOfPlanets);
        }

        private void Quit()
		{
			Application.Quit();
		}
	}
}