﻿using UnityEngine;
using BeyondLight.UI;

namespace BeyondLight
{
	public class Universe : MonoBehaviour
	{
		[SerializeField]
		private GameObject star;

		[SerializeField]
		private int numberOfStars = 300;
		[SerializeField]
		private int maxRadius = 10000;
		[SerializeField]
		private int seedNumber = 100;
		[SerializeField]
		private int minNumberOfPlanets = 1;
		[SerializeField]
		private int maxNumberOfPlanets = 10;

		[SerializeField]
		private float minStarSize = 1f;
		[SerializeField]
		private float maxStarSize = 10f;
		[SerializeField]
		private float minDistBetweenStars = 10f;
		
		private Transform starParent;

		private bool OpenView { get; set; }

		public static Universe UniverseInstance;

		void Awake()
		{
			starParent = GameObject.Find("Lod1").transform;

			seedNumber = MainMenu.seedNumber;
			maxRadius = MainMenu.radius;
			numberOfStars = MainMenu.nrOfStars;
			minStarSize = MainMenu.minStarSize;
			maxStarSize = MainMenu.maxStarSize;
			minNumberOfPlanets = MainMenu.minNrOfPlanets;
			maxNumberOfPlanets = MainMenu.maxNrOfPlanets;
			minDistBetweenStars = MainMenu.minDistanceBetweenStars;

			Random.InitState(seedNumber);

			OpenView = true;

			// Count how many times a star fails to be added.
			int failCount = 0;

			// Generate copies of the star at random positions inside our universe range.
			for (int i = 0; i < numberOfStars; i++)
			{
				Vector3 cartPosition = RandomPosition();
				float radius = Random.Range(minStarSize, maxStarSize);
				StarDummy starDummy = new StarDummy("Star" + i, Random.Range(minNumberOfPlanets, maxNumberOfPlanets), radius);
				Debug.Log("Created " + starDummy.starName + " with " + starDummy.numberOfPlanets + " planets and " + starDummy.radius + " radius!");

				// Create an array of colliders that overlap with our star range.
				float overlapDistance = starDummy.radius + minDistBetweenStars;
				Collider[] positionCollider = Physics.OverlapSphere(cartPosition, overlapDistance);

				// Add the star if it doesn't overlap with others.
				if (positionCollider.Length == 0)
				{
					CreateStarObject(starDummy, cartPosition);
					failCount = 0;
				}
				// Try again if it overlaps with others.
				else
				{
					i--;
					Debug.Log("Oops.");
					failCount++;
				}

				// If the distance between the stars is too big, break the loop in order to avoid crash.
				if (failCount > numberOfStars)
				{
					Debug.LogError("Could not fit all the stars. Distance between them is too big!");
					break;
				}
			}
		}

		private void OnEnable()
		{
			UniverseInstance = this;
		}

		// Generate a copy of the star prefab.
		private void CreateStarObject(StarDummy starDummy, Vector3 position)
		{
			GameObject starCreated = Instantiate(star, position, Quaternion.identity);
			starCreated.transform.parent = starParent.transform;
			var starData = starCreated.GetComponentInChildren<Star>();
			starData.Radius = starDummy.radius;
			starData.NumberOfPlanets = starDummy.numberOfPlanets;
			starCreated.name = starDummy.starName;
		}

		// Convert random spherical coordinates into cartesian coordinates in order to obtain a random position in the universe.
		private Vector3 RandomPosition()
		{
			float distance = Random.Range(0, maxRadius);
			float polarAngle = Random.Range(0, 2 * Mathf.PI);
			float sphericalAngle = Random.Range(0, 2 * Mathf.PI);

			Vector3 cartPosition = new Vector3(distance * Mathf.Cos(polarAngle) * Mathf.Sin(sphericalAngle), distance * Mathf.Cos(sphericalAngle), distance * Mathf.Sin(polarAngle) * Mathf.Sin(sphericalAngle));

			return cartPosition;
		}
	}
}