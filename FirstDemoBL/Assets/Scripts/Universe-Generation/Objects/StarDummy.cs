﻿using UnityEngine;

namespace BeyondLight
{
	public class StarDummy
	{
		public string starName { get; set; }
		public int numberOfPlanets { get; set; }
		public float radius { get; set; }

		public StarDummy(string name, int numberOfPlanets, float radius)
		{
			this.starName = name;
			this.numberOfPlanets = numberOfPlanets;
			this.radius = radius;
		}
	}
}