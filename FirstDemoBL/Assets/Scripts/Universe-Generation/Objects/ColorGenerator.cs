﻿using UnityEngine;

namespace BeyondLight
{
	public class ColorGenerator
	{
		private const int TEXTURE_RESOLUTION = 50;

		private ColorSettings settings;
		private Texture2D texture;

		public void UpdateSettings(ColorSettings settings)
		{
			this.settings = settings;
			if (texture == null)
			{
				texture = new Texture2D(TEXTURE_RESOLUTION, 1);
			}
		}

		public void UpdateElevation(MinMax elevationMinMax)
		{
			settings.planetMaterial.SetVector("_elevationMinMax", new Vector4(elevationMinMax.Min, elevationMinMax.Max));
		}

		public void UpdateColors()
		{
			Color[] colors = new Color[TEXTURE_RESOLUTION];
			for (int i = 0; i < TEXTURE_RESOLUTION; i++)
			{
				colors[i] = settings.gradient.Evaluate(i / (TEXTURE_RESOLUTION - 1f));
			}
			texture.SetPixels(colors);
			texture.Apply();
			settings.planetMaterial.SetTexture("_texture", texture);
		}
	}
}