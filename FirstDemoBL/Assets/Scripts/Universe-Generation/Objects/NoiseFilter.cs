﻿using UnityEngine;

namespace BeyondLight
{
	public class NoiseFilter
	{
		private NoiseSettings settings;
		private Noise noise = new Noise();

		public NoiseFilter(NoiseSettings settings)
		{
			this.settings = settings;
		}

		public float Evaluate(Vector3 point)
		{
			float noiseValue = (noise.Evaluate(point * settings.roughness + settings.centre) + 1) * .5f;
			float frequency = settings.baseRoughness;
			float amplitude = 1;

			for (int i = 0; i < settings.numLayers; i++)
			{
				float v = noise.Evaluate(point * frequency + settings.centre);
				noiseValue += (v + 1) * .5f * amplitude;
				frequency *= settings.roughness;
				amplitude *= settings.persistence;
			}

			noiseValue = Mathf.Max(0, noiseValue - settings.minValue);
			return noiseValue * settings.strength;
		}
	}
}