﻿using UnityEngine;

namespace BeyondLight
{
	public class ShapeGenerator
	{
		private ShapeSettings settings;
		private NoiseFilter[] noiseFilters;
		public MinMax elevationMinMax;

		public void UpdateSettings(ShapeSettings settings)
		{
			this.settings = settings;
			noiseFilters = new NoiseFilter[settings.noiseLayers.Length];
			for (int i = 0; i < noiseFilters.Length; i++)
			{
				noiseFilters[i] = new NoiseFilter(settings.noiseLayers[i].noiseSettings);
			}
			elevationMinMax = new MinMax();
		}

		public Vector3 CalculatePointOnPlanet(Vector3 pointOnUnitSphere, float radius, bool planet)
		{
			if (planet)
			{
				float firstLayerValue = 0;
				float elevation = 0;

				if (noiseFilters.Length > 0)
				{
					firstLayerValue = noiseFilters[0].Evaluate(pointOnUnitSphere);
					if (settings.noiseLayers[0].enabled)
					{
						elevation = firstLayerValue;
					}

				}

				for (int i = 1; i < noiseFilters.Length; i++)
				{
					if (settings.noiseLayers[i].enabled)
					{
						float mask = (settings.noiseLayers[i].useFirstLayerAsMask) ? firstLayerValue : 1;
						elevation += noiseFilters[i].Evaluate(pointOnUnitSphere) * mask;
					}
				}

				elevation = radius * (1 + elevation);
				elevationMinMax.AddValue(elevation);
				return pointOnUnitSphere * elevation;
			}
			return pointOnUnitSphere * radius;
		}
	}
}