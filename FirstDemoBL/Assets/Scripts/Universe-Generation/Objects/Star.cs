﻿using UnityEngine;

namespace BeyondLight
{
	public class Star : MonoBehaviour
	{
		private const int LOD1 = 9;
		private const int DISTANCE_FROM_STAR = 750;
		private const int MIN_DISTANCE_FROM_STAR = 100;

		[SerializeField]
		private GameObject planet;

		[HideInInspector]
		public float Radius;
		[HideInInspector]
		public int NumberOfPlanets;

		[SerializeField]
		private Material mat;
		private Material starMat;

		private ParticleSystem[] starParticles;
		private ShapeGenerator shapeGenerator = new ShapeGenerator();
		private ColorGenerator colorGenerator = new ColorGenerator();
		private MeshFilter[] meshFilters;
		private TerrainFace[] terrainFaces;
		private GameObject star;

		public void Start()
		{
			starMat = new Material(mat);
			GenerateStar();
		}

		private void GenerateStar()
		{
			CreateStar();
			AdjustParticles();
			CreatePlanets();
		}

		private void AdjustParticles()
		{
			// Adjusting particles to the size of the object
			starParticles = GetComponentsInChildren<ParticleSystem>();
			foreach (ParticleSystem p in starParticles)
			{
				p.Play();
				var shape = p.shape;
				var size = p.main.startSize;

				shape.radius = Radius;
				if (p.name == "StarGlowOuter")
				{
					size.constantMin = 90;
					size.constantMax = 100;
				}
			}
		}

		private void CreateStar()
		{
			star = GameObject.CreatePrimitive(PrimitiveType.Sphere);
			Destroy(star.GetComponent<SphereCollider>());
			star.name = "StarBody";
			star.transform.localScale = new Vector3(Radius*2, Radius*2, Radius*2);
			star.transform.parent = transform;
			star.transform.position = transform.position;
			var starMeshRenderer = star.transform.GetComponent<MeshRenderer>();
			starMeshRenderer.material = starMat;
			star.layer = LOD1;
		}

		private void CreatePlanets()
		{
			var position = transform.position;
			float x;
			var planetPosition = new Vector3();

			for (int i = 0; i < NumberOfPlanets; i++)
			{
				do
				{
					x = Random.Range(position.x - DISTANCE_FROM_STAR, position.x + DISTANCE_FROM_STAR);
				}
				while (x > position.x - MIN_DISTANCE_FROM_STAR && x < position.x + MIN_DISTANCE_FROM_STAR);

				//float y = Random.Range(position.y - 500, position.y + 500);
				float z = Random.Range(position.z - DISTANCE_FROM_STAR, position.z + DISTANCE_FROM_STAR);

				planetPosition.x = x;
				planetPosition.y = position.y;
				planetPosition.z = z;

				GameObject planetCreated = Instantiate(planet, planetPosition, Quaternion.identity);
				
				planetCreated.transform.parent = transform;
			}
		}

		private void GenerateMesh()
		{
			foreach (TerrainFace face in terrainFaces)
			{
				face.ConstructMesh();
			}
		}

		private void GenerateColors()
		{
			Color32 c = new Color32(
			(byte)Random.Range(200, 255),
			(byte)Random.Range(230, 255),
			(byte)Random.Range(100, 255),
			255
			);

			Gradient g = new Gradient();
			starMat.EnableKeyword("_EMISSION");
			starMat.SetColor("_EmissionColor", c);
			
			foreach (MeshFilter m in meshFilters)
			{
				m.GetComponent<MeshRenderer>().sharedMaterial = starMat;
				//m.GetComponent<MeshRenderer>().sharedMaterial.color = c;
			}
		}
	}
}