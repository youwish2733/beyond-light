﻿using UnityEngine;

namespace BeyondLight
{
	public class Planet : MonoBehaviour
	{
		private const int NUMBER_OF_LAYERS = 3;
		private const int LOD1 = 9;
		private const float BASE_REVOLUTION_SPEED = 15.0f;
		private const float ROTATION_SPEED = 0.03f;

		[SerializeField]
		public ShapeSettings ShapeSettings;
		[SerializeField]
		public ColorSettings ColorSettings;

		[SerializeField]
		private Material planetMat;

		[SerializeField]
		[Range(2, 256)]
		private int resolution = 10;
		[SerializeField]
		private bool autoUpdate = true;
		[SerializeField]
		private float minRadius = 3;
		[SerializeField]
		private float maxRadius = 10;
		private float revolutionSpeed;
		private NoiseSettings noiseSettings;
		private ShapeGenerator shapeGenerator = new ShapeGenerator();
		private ColorGenerator colorGenerator = new ColorGenerator();
		private MeshFilter[] meshFilters;
		private TerrainFace[] terrainFaces;
		private ParticleSystem[] planetParticles;
		private Transform starPivot;

		private float radius;

		public void Start()
		{
			GeneratePlanet();

			starPivot = transform.parent;
			revolutionSpeed = BASE_REVOLUTION_SPEED * starPivot.localScale.x / Vector3.Distance(starPivot.position, transform.position);
		}

		void Update()
		{
			transform.RotateAround(starPivot.position, Vector3.up, revolutionSpeed * Time.deltaTime);
			transform.RotateAround(transform.position, Vector3.up, ROTATION_SPEED * radius);
		}

		public void GeneratePlanet()
		{
			RandomizeSettings();
			Initialize();
			GenerateMesh();
			GenerateColors();
		}

		private void Initialize()
		{
			radius = Random.Range(minRadius, maxRadius);

			ColorSettings.planetMaterial = new Material(planetMat);

			shapeGenerator.UpdateSettings(ShapeSettings);
			colorGenerator.UpdateSettings(ColorSettings);

			if (meshFilters == null || meshFilters.Length == 0)
			{
				meshFilters = new MeshFilter[6];
			}

			terrainFaces = new TerrainFace[6];

			Vector3[] directions = { Vector3.up, Vector3.down, Vector3.left, Vector3.right, Vector3.forward, Vector3.back };

			for (int i = 0; i < 6; i++)
			{
				if (meshFilters[i] == null)
				{
					GameObject meshObj = new GameObject("Planet mesh");
					meshObj.transform.parent = transform;

					meshObj.AddComponent<MeshRenderer>();
					meshFilters[i] = meshObj.AddComponent<MeshFilter>();
					meshFilters[i].sharedMesh = new Mesh();
					meshFilters[i].transform.position = transform.position;
				}

				meshFilters[i].GetComponent<MeshRenderer>().sharedMaterial = ColorSettings.planetMaterial;
				meshFilters[i].GetComponent<MeshRenderer>().renderingLayerMask = 3;
				terrainFaces[i] = new TerrainFace(shapeGenerator, meshFilters[i].sharedMesh, resolution, directions[i], radius, true);
				//meshFilters[i].GetComponent<MeshRenderer>().enabled = false;
			}
			Transform[] children = transform.GetComponentsInChildren<Transform>();
			foreach (Transform go in children)
			{
				go.gameObject.layer = LOD1;
			}
		}

		/// <summary>
		/// Randomize generated mesh values.
		/// </summary>
		private void RandomizeSettings()
		{
			ShapeSettings.NoiseLayer[] noiseLayers = new ShapeSettings.NoiseLayer[NUMBER_OF_LAYERS];

			float strength = Random.Range(.01f, .02f);
			float roughness = Random.Range(1f, 2f);
			float baseRoughness = Random.Range(0.15f, 0.8f);
			int numLayers = 5;
			float persistence = Random.Range(0.5f, 0.7f);
			float minValue = Random.Range(1.5f, 2f);
			Vector3 centre = new Vector3(Random.Range(0f, 10f), Random.Range(0f, 10f), Random.Range(0f, 10f));

			noiseLayers[0] = new ShapeSettings.NoiseLayer();
			noiseLayers[0].useFirstLayerAsMask = false;
			noiseLayers[0].noiseSettings = new NoiseSettings(strength, roughness, centre, baseRoughness, numLayers, persistence, minValue);

			strength = Random.Range(.1f, 0.2f);
			roughness = Random.Range(1.5f, 2f);
			baseRoughness = Random.Range(0.5f, 1f);
			numLayers = 5;
			persistence = Random.Range(1f, 1.4f);
			minValue = Random.Range(2f, 2.5f);
			centre = transform.position;

			noiseLayers[1] = new ShapeSettings.NoiseLayer();
			noiseLayers[1].noiseSettings = new NoiseSettings(strength, roughness, centre, baseRoughness, numLayers, persistence, minValue);

			strength = Random.Range(.2f, .3f);
			roughness = Random.Range(2.5f, 4f);
			baseRoughness = Random.Range(1f, 1.5f);
			numLayers = 4;
			persistence = Random.Range(1f, 1.4f);
			minValue = Random.Range(2f, 2.5f);
			centre = transform.position;

			noiseLayers[2] = new ShapeSettings.NoiseLayer();
			noiseLayers[2].noiseSettings = new NoiseSettings(strength, roughness, centre, baseRoughness, numLayers, persistence, minValue);

			ShapeSettings = ScriptableObject.CreateInstance<ShapeSettings>();
			ShapeSettings.noiseLayers = noiseLayers;
		}

		public void OnShapeSettingsUpdated()
		{
			Initialize();
			GenerateMesh();
		}

		public void OnColorSettingsUpdated()
		{
			Initialize();
			GenerateColors();
		}

		private void GenerateMesh()
		{
			foreach (TerrainFace face in terrainFaces)
			{
				face.ConstructMesh();
			}

			colorGenerator.UpdateElevation(shapeGenerator.elevationMinMax);
		}

		private void GenerateColors()
		{
			colorGenerator.UpdateColors();
		}

		private void OnDrawGizmos()
		{
			if (this.ColorSettings.planetMaterial.GetTexture("_texture") == null)
			{
				colorGenerator.UpdateSettings(ColorSettings);
				colorGenerator.UpdateColors();
			}
		}
	}
}