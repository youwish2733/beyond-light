﻿using UnityEngine;

namespace BeyondLight
{
	[System.Serializable]
	public class NoiseSettings
	{
		public float strength;
		public float roughness;
		[Range(1, 8)]
		public int numLayers;
		public float baseRoughness = 1f;
		public float persistence = .5f;
		public Vector3 centre;
		public float minValue;

		public NoiseSettings(float strength, float roughness, Vector3 centre, float baseRoughness, int numLayers, float persistence, float minValue)
		{
			this.strength = strength;
			this.roughness = roughness;
			this.centre = centre;
			this.baseRoughness = baseRoughness;
			this.numLayers = numLayers;
			this.persistence = persistence;
			this.minValue = minValue;
		}
	}
}