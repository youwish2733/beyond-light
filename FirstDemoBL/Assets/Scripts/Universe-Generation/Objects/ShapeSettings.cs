﻿using System;
using UnityEngine;

namespace BeyondLight
{
	[CreateAssetMenu()]
	public class ShapeSettings : ScriptableObject
	{
		public NoiseLayer[] noiseLayers;

		public ShapeSettings(NoiseLayer[] noiseLayers)
		{
			this.noiseLayers = noiseLayers;
		}

		[Serializable]
		public class NoiseLayer
		{
			public bool enabled = true;
			public bool useFirstLayerAsMask = true;
			public NoiseSettings noiseSettings;
		}
	}
}