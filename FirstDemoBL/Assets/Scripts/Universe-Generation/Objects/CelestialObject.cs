﻿using UnityEngine;

namespace BeyondLight
{
	public class CelestialObject : MonoBehaviour
	{
		[SerializeField]
		private Material mat;
		private Material starMat;
		[SerializeField]
		[Range(2, 256)]
		private int resolution = 10;
		[SerializeField]
		private bool autoUpdate = true;
		ParticleSystem[] starParticles;


		public ShapeSettings shapeSettings;
		public ColorSettings colorSettings;

		ShapeGenerator shapeGenerator = new ShapeGenerator();
		ColorGenerator colorGenerator = new ColorGenerator();

		[SerializeField, HideInInspector] MeshFilter[] meshFilters;
		TerrainFace[] terrainFaces;

		public void Start()
		{
			starMat = new Material(mat);
			GenerateStar();
		}

		public void Initialize()
		{
			float radius = transform.localScale.x;
			starParticles = GetComponentsInChildren<ParticleSystem>();
			foreach (ParticleSystem p in starParticles)
			{
				p.Play();
				var shape = p.shape;
				var size = p.main.startSize;

				shape.radius = radius + 2;
				if (p.name == "StarGlowOuter")
				{
					size.constantMin = 90;
					size.constantMax = 100;
				}
				//else
				//{
				//size = 5555;
				//}
			}

			shapeGenerator.UpdateSettings(shapeSettings);
			colorGenerator.UpdateSettings(colorSettings);
			if (meshFilters == null || meshFilters.Length == 0)
			{
				meshFilters = new MeshFilter[6];
			}
			terrainFaces = new TerrainFace[6];

			Vector3[] directions = { Vector3.up, Vector3.down, Vector3.left, Vector3.right, Vector3.forward, Vector3.back };

			for (int i = 0; i < 6; i++)
			{
				if (meshFilters[i] == null)
				{
					GameObject meshObj = new GameObject("Star mesh");
					meshObj.transform.parent = transform;

					meshObj.AddComponent<MeshRenderer>().sharedMaterial = new Material(Shader.Find("Standard"));
					meshFilters[i] = meshObj.AddComponent<MeshFilter>();
					meshFilters[i].sharedMesh = new Mesh();
					meshFilters[i].transform.position = transform.position;
				}
				terrainFaces[i] = new TerrainFace(shapeGenerator, meshFilters[i].sharedMesh, resolution, directions[i], radius, false);
				meshFilters[i].GetComponent<MeshRenderer>().enabled = false;
			}
			Transform[] children = transform.GetComponentsInChildren<Transform>();
			foreach (Transform go in children)
			{
				go.gameObject.layer = 9;
			}
		}

		public void GenerateStar()
		{
			Initialize();
			GenerateMesh();
			GenerateColors();
		}

		public void OnShapeSettingsUpdated()
		{
			if (autoUpdate)
			{
				Initialize();
				GenerateMesh();
			}
		}

		public void OnColorSettingsUpdated()
		{
			if (autoUpdate)
			{
				Initialize();
				GenerateColors();
			}
		}

		void GenerateMesh()
		{
			foreach (TerrainFace face in terrainFaces)
			{
				face.ConstructMesh();
			}
		}

		void GenerateColors()
		{
			Color32 c = new Color32(
			(byte)Random.Range(200, 255),
			(byte)Random.Range(230, 255),
			(byte)Random.Range(100, 255),
			255
			);

			Gradient g = new Gradient();
			starMat.EnableKeyword("_EMISSION");
			starMat.SetColor("_EmissionColor", c);

			colorSettings.planetMaterial = starMat;
			foreach (MeshFilter m in meshFilters)
			{
				m.GetComponent<MeshRenderer>().sharedMaterial = starMat;
				//m.GetComponent<MeshRenderer>().sharedMaterial.color = c;
			}
		}
	}
}