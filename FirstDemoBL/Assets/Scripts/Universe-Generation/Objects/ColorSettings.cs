﻿using UnityEngine;

namespace BeyondLight
{
	[CreateAssetMenu()]
	public class ColorSettings : ScriptableObject
	{
		public Gradient gradient;
		public Material planetMaterial;
	}
}