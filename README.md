# Beyond Light

Beyond Light is a space simulation game, where the concepts like multi-scale representation, floating origin, and procedural content generation are used to provide the user with a realistic visual experience of space exploration in a virtual universe. The user will be able to navigate through space with a spaceship to discover and experience realistic representations of celestial objects, such as stars, planets etc.

# Features

Description: The user can control a spaceship to travel between solar systems, and get up close to the stars and planets.

* The user is presented with a menu where the universe generation can be configured.
* Spaceship zero-g spaceflight: camera and movement.
* Stars systems.
* Unique planets with procedural terrain.

# Instructions

Click on `Clone or download` -> `Download ZIP` -> Extract files -> `Beyond Light` -> run `FirstDemoBL.exe`.

# User Manual

Use alt + click to pan around with the camera.

Use mouse scroll button to zoom in and out.

Use arrow buttons to accelerate, decelerate, and turn.

Use `w`, `a`, `s`, `d` to pitch and roll.

More updates to come!
